# -*- coding: utf-8 -*-
"""
Spyder Editor
print("lenA: %d" % lenA)
This is a temporary script file.
"""
import numpy as np

def anyPositives(M):
    lenA = (len(M[0])-1)/2
    for i in range(1,lenA+1):
        if(M[0][i] > 0):
            return True
    return False

def findLowestRow(M):
    rows = len(M)
    cols = len(M[0])
    lenA = (cols-1)/2
    for i in range(1,lenA+1):
        iSum = 0
        lowest = 0
        lowestAt = -1
        j = 1
        while j < rows:
            if not M[i][j] == 0:
                iSum += 1
            elif (lowestAt == -1 or M[i][cols-1]/M[i][j] < lowest):
                lowestAt = j
                lowest = M[i][j]
            j += 1
        
        if(iSum > 1):
            return i,j
        
    print("No lowest row found.")

def allNegativeNumbers(M):
    cols = len(M[0])
    lenA = (cols-1)/2
    for j in range (1,lenA+1):
        if M[0][j] > 0:
            return False
    return True
        

string = "maksimer 4x+5y+6z"

ct = [2,3,4,9]
A = [[3,2,1],[2,5,3]]
b = [0,10,15]

I=[[1,0,0],[0,1,0],[0,0,1]]

M = np.array([[ 1, 2, 3, 4, 0, 0, 0],
              [ 0, 3, 2, 1, 1, 0,10],
              [ 0, 2, 5, 3, 0, 1,15]])
cols = len(M[0])

while not allNegativeNumbers(M):
    
    M[0] = M[0] * M[2][3]
    M[1] = M[1] * M[2][3]
    
    M[0] = M[0]+(-1*M[2]*(M[0][3]/M[2][3]))
    M[1] = M[1]+(-1*M[2]*(M[1][3]/M[2][3]))
    
    if not anyPositives(M):
        so = M[0][cols-1]/M[0][0]
        print('solution: %d' % so)

